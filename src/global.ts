import type { JWT } from '@auth/core/jwt';
import { env } from '$env/dynamic/private';
import { decode } from '@auth/core/jwt';
import type { UserData } from './stores';
import jwt from 'jsonwebtoken';
import { error } from '@sveltejs/kit';

export async function extractUserData(token: JWT) {
	const userdata: UserData = (token?.picture as any).data;
	return userdata;
}

export async function isController(token: JWT | null): Promise<boolean> {
	if (token == null || !token) return false;
	const userdata: UserData = (token?.picture as any).data;
	if (userdata.vatsim.subdivision.id != 'POR') return false;
	if (userdata.vatsim.rating.id < 1) return false;
	return true;
}

export async function decodeSessionToken(sessionToken: any): Promise<JWT | null> {
	const token = await decode({
		token: sessionToken,
		secret: env.AUTH_SECRET,
		salt: env.SESSION_TOKEN_COOKIE_NAME
	});
	return token;
}

