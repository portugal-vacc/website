import { readable, writable } from 'svelte/store';

interface vatsimEvent {
	name: string;
	start_time: string;
	end_time: string;
	short_description: string;
	description: string;
	id: number;
	banner: string;
}

interface onlineController {
	name: string;
	callsign: string;
	frequency: string;
	logon_time: number;
	facility: number;
}

interface urlData {
	pathname: string;
	params: URLSearchParams;
}

export interface UserData {
	cid: string;
	personal: {
		name_first: string;
		name_last: string;
		name_full: string;
		email: string;
		country: { id: string; name: string };
	};
	vatsim: {
		rating: { id: number; long: string; short: string };
		pilotrating: { id: number; long: string; short: string };
		division: { id: string; name: string };
		region: { id: string; name: string };
		subdivision: { id: string; name: string };
	};
}

export interface UserData_Response extends UserData {
	is_controller: boolean;
}

function timestampToUnix(dateString: string): number {
	const unix = new Date(dateString).getTime() / 1000;
	return unix;
}

export const vatsimEvents = writable(new Array<vatsimEvent>());
export const onlineControllers = (() => {
	const { subscribe, set, update } = writable(new Array<onlineController>());
	return {
		subscribe: subscribe,
		set: set,
		update: update,
		refresh: async () => {
			const _vatsimEvents = (await new Promise((resolve, reject) => {
				fetch('https://vatdata.vatsim.pt/api/v1/events/portugal', {
					method: 'GET'
				})
					.then((response) => response.json())
					.then((result) => resolve(result))
					.catch((error) => reject(error));
			})) as vatsimEvent[];
			vatsimEvents.update((n) => _vatsimEvents);

			var controllers: onlineController[] = [];
			var rawControllers = await (
				await fetch('https://vatdata.vatsim.pt/api/v1/live/controllers/LP')
			).json();

			for (let controller of rawControllers) {
				controllers.push({
					name: controller.name,
					callsign: controller.callsign,
					frequency: controller.frequency,
					logon_time: timestampToUnix(controller.logon_time),
					facility: controller.facility
				});
			}
			set(controllers.filter((controller) => controller.facility > 0));
		}
	};
})();

export async function getUserData(): Promise<UserData | null> {
	var response = await fetch('/api/v1/sessiondata', { credentials: 'include' });
	if (response.ok) return await response.json();
	else return null;
}

export const minimizeNavbar = writable<boolean>(false);
export const hideNavbar = writable<boolean>(false);
export const showFooter = writable<boolean>(true);
export const session = writable<any | null>(null);
export const bodyscroll = writable<boolean>(true);
export const url = writable<urlData>();
export const airportIcaos = readable([
	'LPPT',
	'LPPR',
	'LPFR',
	'LPAZ',
	'LPBG',
	'LPBJ',
	'LPCR',
	'LPCS',
	'LPFL',
	'LPGR',
	'LPHR',
	'LPLA',
	'LPMA',
	'LPPD',
	'LPPI',
	'LPPS',
	'LPSJ',
	'LPVR',
	'LPVL',
	'LPCB',
	'ENROUTE'
]);
