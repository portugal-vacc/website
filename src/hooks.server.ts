import { SvelteKitAuth } from '@auth/sveltekit';
import { env } from '$env/dynamic/private';

export interface VatsimProfile extends Record<string, any> {
	cid: string;
	name: string;
	email: string;
}

export const { handle, signIn, signOut } = SvelteKitAuth({
	secret: env.AUTH_SECRET,
	callbacks: {
		async jwt({ token, account, profile, trigger }) {
			// if (trigger == 'signIn') console.log({ token, account, profile, trigger });
			if (account) {
				token.accessToken = account.access_token;
				token.id = profile?.id;
			}
			return token;
		}
	},
	providers: [
		{
			id: 'vatsim',
			name: 'VATSIM',
			type: 'oauth',
			authorization: `${env.VATSIM_AUTH_ADDRESS}/oauth/authorize?scope=full_name%20email%20vatsim_details%20country`,
			token: `${env.VATSIM_AUTH_ADDRESS}/oauth/token`,
			clientId: env.VATSIM_ID,
			clientSecret: env.VATSIM_SECRET,
			userinfo: `${env.VATSIM_AUTH_ADDRESS}/api/user`,
			profile(profile) {
				return {
					name: profile.data?.personal?.name_full,
					email: profile.data?.personal?.email,
					image: profile
				};
			},
			style: { logo: '', bg: '#26AF6F', text: '#fff' }
		}
	],
	trustHost: true
});
