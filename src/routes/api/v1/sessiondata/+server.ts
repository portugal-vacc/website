import { env } from '$env/dynamic/private';
import { decode } from '@auth/core/jwt';
import { decodeSessionToken, isController } from '../../../../global.js';
import type { session, UserData_Response } from '../../../../stores.js';

export async function GET({ request, url, getClientAddress, cookies }) {
	const sessionToken: any = cookies.get(env.SESSION_TOKEN_COOKIE_NAME);
	const token = await decodeSessionToken(sessionToken);
	if (!token) return new Response('NO TOKEN', { status: 401 });
	const userdata = (token?.picture as any).data;
	if (userdata) {
		let userdata_response: UserData_Response = userdata;
		userdata_response.is_controller = await isController(token);
		return new Response(JSON.stringify(userdata_response));
	} else return new Response('NO USERDATA', { status: 401 });
}
