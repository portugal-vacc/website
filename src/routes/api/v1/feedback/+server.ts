import { env } from '$env/dynamic/private';
import { decodeSessionToken } from '../../../../global.js';

export async function GET({ request, url, getClientAddress, cookies }) {
	const sessionToken: any = cookies.get(env.SESSION_TOKEN_COOKIE_NAME);

	const jwt_token = await decodeSessionToken(sessionToken);

	var discordRequest_Headers = new Headers();
	discordRequest_Headers.append('Content-Type', 'application/json');

	var title = url.searchParams.get('title');
	var description = url.searchParams.get('description');

	if (!(title && description && jwt_token != null && jwt_token))
		return new Response(null, { status: 401 });

	// const user_token = await vatsim_convertAuthCode(grant_code);
	// if (!user_token) return new Response(null, { status: 401 });
	const userinfo = (jwt_token!.picture as any).data;

	const discordRequest_Body = {
		content: `<@&${env.DISCORD_FEEDBACK_ROLE_ID}>`,
		embeds: [
			{
				title: title,
				//color: 0x2b2d31,
				color: 0x1abb9b,
				fields: [
					{ name: 'Description', value: description, inline: false },
					{ name: 'Name', value: userinfo!.personal.name_full, inline: true },
					{ name: 'E-mail Address', value: userinfo!.personal.email, inline: true },
					{ name: 'CID', value: userinfo!.cid, inline: true }
				],
				footer: {
					text: `Vatsim.pt • Report received at ${new Date().toISOString()} from IP address ${getClientAddress()}`
				}
			}
		]
	};

	await fetch(env.DISCORD_FEEDBACK_WEBHOOK_URL, {
		method: 'POST',
		headers: discordRequest_Headers,
		body: JSON.stringify(discordRequest_Body)
	});

	return new Response(String('Success'));
}
