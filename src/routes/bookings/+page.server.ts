import { env } from '$env/dynamic/private';
import { redirect } from '@sveltejs/kit';
import { decodeSessionToken } from '../../global.js';

export async function load({ cookies, locals }) {
	const sessionToken: any = cookies.get(env.SESSION_TOKEN_COOKIE_NAME);
	const token = await decodeSessionToken(sessionToken);
	return {
		session: token,
		bookings_address: env.BOOKINGS_API_ADDRESS
	};
}
