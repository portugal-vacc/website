import { redirect } from '@sveltejs/kit';
import type { RouteParams } from './$types';
import { vatsimEvents } from '../../../stores';

export function load({ params }: { params: RouteParams }) {
	return {
		eventid: parseInt(params.eventid)
	};
}
