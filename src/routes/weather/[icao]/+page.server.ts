import { redirect } from '@sveltejs/kit';
import type { RouteParams } from './$types';

// async function fetchMetars() {
// 	let xmlToJson = (xml: string) => {
// 		let jsonObj = null;
// 		const parser = new xml2js.Parser({
// 			explicitArray: false,
// 			mergeAttrs: true
// 		});

// 		parser.parseString(xml, (err, result) => {
// 			if (err) {
// 				console.error('Error parsing XML:', err);
// 			} else {
// 				jsonObj = result;
// 			}
// 		});

// 		return jsonObj;
// 	};

// 	var xml = (await new Promise((resolve, reject) => {
// 		fetch('https://www.aviationweather.gov/adds/dataserver_current/current/metars.cache.xml')
// 			.then((response) => response.text())
// 			.then(async (response) => {
// 				resolve(response);
// 			});
// 	})) as string;

// 	return (xmlToJson(xml) as any).response.data.METAR;
// }

// async function fetchTafs() {
// 	let xmlToJson = (xml: string) => {
// 		let jsonObj = null;
// 		const parser = new xml2js.Parser({
// 			explicitArray: false,
// 			mergeAttrs: true
// 		});

// 		parser.parseString(xml, (err, result) => {
// 			if (err) {
// 				console.error('Error parsing XML:', err);
// 			} else {
// 				jsonObj = result;
// 			}
// 		});

// 		return jsonObj;
// 	};

// 	var xml = (await new Promise((resolve, reject) => {
// 		fetch('https://www.aviationweather.gov/adds/dataserver_current/current/tafs.cache.xml')
// 			.then((response) => response.text())
// 			.then(async (response) => {
// 				resolve(response);
// 			});
// 	})) as string;

// 	return (xmlToJson(xml) as any).response.data.TAF;
// }

// async function getMetar(icao: string): Promise<string> {
// 	return ((await fetchMetars()) as any[]).find((metar) => icao === metar.station_id)?.raw_text;
// }

// async function getTaf(icao: string): Promise<any> {
// 	const out = ((await fetchTafs()) as any[]).find((taf) => icao === taf.station_id)?.raw_text;
// 	return out;
// 	// return await fetchTafs();
// }

// export async function load({ params }: { params: RouteParams }) {
// 	if (params.icao !== params.icao.toUpperCase()) {
// 		throw redirect(307, '/airports/' + params.icao.toUpperCase());
// 	}
// 	const rawMetar = await getMetar(params.icao);
// 	// const rawMetar =
// 	// 	'METAR EHLE 280925Z AUTO 21009G19KT 060V130 5000 -RA FEW007 BKN014CB BKN017 R23/0500 02/M01 Q1001 BECMG 6000';

// 	const rawTaf = await getTaf(params.icao);
// 	return {
// 		icao: params.icao,
// 		metar: rawMetar ? metar_taf_parser.parseMetar(rawMetar) : null,
// 		taf: rawTaf ? metar_taf_parser.parseTAF(rawTaf) : null
// 	};
// }

export async function load({ params }: { params: RouteParams }) {
	if (params.icao !== params.icao.toUpperCase()) {
		redirect(307, '/weather/' + params.icao.toUpperCase());
	}
	return {
		icao: params.icao
	};
}
