import { redirect } from '@sveltejs/kit';
import type { RouteParams } from './$types';

export function load({ params }: { params: RouteParams }) {
	if (params.icao !== params.icao.toUpperCase()) {
		redirect(307, '/airports/' + params.icao.toUpperCase());
	}
	return {
		icao: params.icao
	};
}
