import { redirect } from '@sveltejs/kit';
import type { RouteParams } from './$types';

export function load({ params }: { params: RouteParams }) {
	if (params.airport !== params.airport.toUpperCase()) {
		redirect(307, '/charts/' + params.airport.toUpperCase());
	}
	return {
		airport: params.airport
	};
}
