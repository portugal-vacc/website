export const load = ({ url }) => {
	return {
		pathname: url.pathname,
		params: url.searchParams
	};
};
