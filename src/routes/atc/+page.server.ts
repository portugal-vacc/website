import { redirect } from '@sveltejs/kit';
import { env } from '$env/dynamic/private';
import { decode, type JWT } from '@auth/core/jwt';
import { decodeSessionToken, isController } from '../../global.js';

export async function load({ cookies, locals }) {
	const sessionToken: any = cookies.get(env.SESSION_TOKEN_COOKIE_NAME);
	const jwt_token = await decodeSessionToken(sessionToken);
	const isAuthorized = await isController(jwt_token);
	if (!isAuthorized) redirect(301, '/signin?redirect_uri=%2Fatc');
}
